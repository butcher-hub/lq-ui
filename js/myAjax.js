//1、参数：  url:请求地址   type:请求方式     data:{}请求参数     dataType:返回数据类型     success: 获得返回数据执行操作     error：请求失败执行操作
//   myAjax({
//      url:xxxx
//      type:get/post
//      data:{
//      key:value
//      ......
//      }
//      dataType:JSON
//      success:function(){}
//      error:function(){}
//   })
function myAjax(obj){
    var xmr = new XMLHttpRequest();
    var newStr = "";
    //处理data
    if(obj.data){
        var str = "";
        for (var key in obj.data) {
           str += "&" + key + "=" + obj.data[key];
        }
        //把开头的&去掉
        newStr = str.slice(1);
    }
    //处理type
    if(obj.type.toLowerCase() == "get"){
        xmr.open("get",obj.url + "?" + newStr,true);
        xmr.send();

    }else if(obj.type.toLowerCase() == "post"){
        xmr.open("post",obj.url,true);
        //设置请求头
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
        xmr.send(newStr);
    }

    //处理基监听函数和返回数据类型
    xmr.onreadystatechange = function(){
        if(xmr.readyState == 4){
            if(xmr.status == 200){
                if(obj.dataType && obj.dataType.toLowerCase == "json"){
                    var JsonText = JSON.parse(xmr.responseText());
                    obj.success(JsonText);
                }else{
                    obj.success(xmr.responseText());
                }
            }else{
                obj.error("执行失败");
            }
        }
    }
   
}